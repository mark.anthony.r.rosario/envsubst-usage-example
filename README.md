# envsubst usage example

## scenario 1 - all variables are specified
this is useful if your config file template has $variables in it, like nginx config files
```bash
# set env vars
export APP_NAME=example_one

# view the output with the template + environment variables
envsubst '$APP_NAME' < one.template.config

# save the output of the template + environment variables to a file
envsubst '$APP_NAME' < one.template.config > one.config

# view the generated config gile
cat one.config
````


## scenario 2- no variables specified (will use all available environment variable)
```bash
# view the output with the template + environment variables
envsubst two.template.config

# save the output of the template + environment variables to a file
envsubst < two.template.config > two.config

# view the generated config gile
cat two.config
```
